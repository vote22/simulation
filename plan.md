# Plan de travail

## Découpage des circonscriptions

Pour le découpage en circonscriptions, deux pistes :
- comme suggéré par le doc de travail assemblée, en conservant un sous-ensemble des circonscriptions et en redistribuant les voix de celles supprimées sur celles adjascentes ; et
- avec un découpage abstrait de la géographie.

Ces deux pistes sont explorées en parallèle pour minimiser les risques d'impasse.


### Regroupement des circonscriptions [SB]

En reprenant les idées de PD et MK (redistributions des voix de certaines circonscriptions sur celles conservées), et aussi en testant des regroupements sur des critères "raisonnables" (i.e. comme les règles utilisées pour le rapport terra nova).
L'idée est qu'avec un code informatique, on puisse éventuellement tester tous les regroupements possibles qui remplissent ces critères "raisonnables" et ainsi avoir une idée de la robustesse chaque système de vote en regardant la variabilité de ses résultats sur l'ensemble des découpages.
Éventuellement, on peut redescendre au niveau canton et tester la combinatoire des regroupements si on a des impasses au niveau circonscription.


### Découpage "abstrait" [RB]

Par abstrait on veut dire ne reprenant pas la géographie électorale.
L'idée est de générer une population électorale et un découpage en circonscriptions qui ne soit pas liés à la géographie actuelle, mais qui en ait les caractéristiques statistiques (pour la population, répartition des opinions politiques identiques au 1er tour de 2017, pour les circonscriptions, distribution des tailles et des biais dans les représentations des opinions politiques calqués sur ce même premier tour).
Le nombre de circonscription devient un paramètre du modèle qu'on peut ajuster sans se soucier de regroupement de circonscriptions réelles.
Pour vérifier la validité du modèle :
- avec N = 577, et le mode de scrutin actuel, on vérifie qu'on retombe sur l'assemblée réelle.
- avec N = 103 et le mode "terra nova", on vérifie qu'on retombe sur le résultat "à la main" de terra nova.

Un avantage de cette méthode et qu'elle découple la problématique du mode de scrutin de la question du découpage, cette seconde question pouvant peut-être parasiter la première.
On imagine un député se penchant sur les simulations, sa première interrogation sera peut-être de savoir si "sa" circonscription est gardée ou non, et selon la réponse, sa perception des modes de scrutins sera peut-être différente.


## Report des voix

Deux pistes là aussi:
- un modèle simple "à la louche" de report parti par parti en considérant les cas possibles de second tours (à discuter avec B. Cautres) ; et
- un modèle de report calculé à partir des données des 2 tours de 2017.
