résultats législatives 2017:
https://www.data.gouv.fr/fr/posts/les-donnees-des-elections/
https://www.data.gouv.fr/en/datasets/elections-legislatives-des-11-et-18-juin-2017-resultats-du-1er-tour/
https://www.data.gouv.fr/en/datasets/elections-legislatives-des-11-et-18-juin-2017-resultats-du-2nd-tour/

contours des circonscriptions:
https://www.data.gouv.fr/fr/datasets/carte-des-circonscriptions-legislatives-2012-et-2017/

contours des cantons:
https://www.data.gouv.fr/fr/datasets/decoupage-des-cantons-pour-les-elections-departementales-de-mars-2015/

contours des départements:
http://professionnels.ign.fr/geofla
