try:  # are we in a module?
    __file__
except NameError:  # if not, change path to root
    import os
    try:
        CWD
    except NameError:
        CWD = os.getcwd()
    os.chdir(os.path.join(CWD, ".."))

from constants import NUANCES


def finalists(t1):
    """Computes the set of candidates that qualify for the 2nd round"""
    indices = list(range(len(NUANCES)))
    inscrits, _, _, _, _, exprimes, *t1 = t1

    # absolute majority?
    finalists = {
        i for i in indices
        if t1[i]/inscrits >= 0.25 and t1[i]/exprimes >= 0.5
    }
    if len(finalists) >= 1:
        return finalists

    # …else, 2 above threshold?
    finalists = {
        i for i in indices
        if t1[i]/inscrits >= 0.125
    }
    if len(finalists) >= 2:
        return finalists

    # … else, 2 best choices:
    return {*sorted(indices, key=lambda i: t1[i], reverse=True)[:2]}


def plurality_with_runoff(circonscriptions, winner_predictor):
    """Computes the plurality with runoff rule. Returns a pair:
        - the list of second rounds
        - the list of winners"""
    t2, winners = {}, {}
    for circo in circonscriptions:
        results = circonscriptions[circo]
        t1 = results["t1"]
        final = finalists(t1)
        t2[circo] = final
        winners[circo] = winner_predictor(t1, final)
    return t2, winners


def nb_elected(winners):
    return [sum(winners[circo] == nuance for circo in winners) for nuance in NUANCES]


def test_finalists():
    from paths import CIRCONSCRIPTIONS_FILE

    INSCRITS, ABSTENTION, VOTANTS, BLANCS, NULS, EXPRIMES = range(6)
    COLUMNS = ["inscrits", "abstention", "votants", "blancs", "nuls", "exprimes"] + NUANCES
    L = len(COLUMNS)
    N = len(NUANCES)

    try:
        results = open(CIRCONSCRIPTIONS_FILE)
    except FileNotFoundError:
        from data.data_loader import load_circonscriptions
        load_circonscriptions()
        results = open(CIRCONSCRIPTIONS_FILE)

    next(results).split()

    for line in results:
        circo, _, _, _, *words = line.split('\t')
        if circo.startswith('Z'):
            continue
        t1, t2 = words[:L], words[L:]
        t1 = [int(u) for u in t1]
        try:
            t2 = [int(u) for u in t2]
        except ValueError:  # no second round
            continue
        comp_finalists = finalists(t1)
        real_finalists = {i for i, v in enumerate(t2[-N:]) if v > 0}
        if comp_finalists == real_finalists:
            continue
        print(circo, "/".join(NUANCES[u] for u in comp_finalists), "vs.",
              "/".join(NUANCES[u] for u in real_finalists))
