try:  # are we in a module?
    __file__
except NameError:  # if not, change path to root
    import os
    try:
        CWD
    except NameError:
        CWD = os.getcwd()
    os.chdir(os.path.join(CWD, ".."))

from constants import NUANCES
from utils.misc import argmax, argtopk


def circonscriptions_to_votes(circonscriptions):
    """Computes the total number of votes for each party."""
    return [sum(c['t1'][n + 6] for c in circonscriptions.values()) for n in range(len(NUANCES))]


def circonscriptions_to_deficits(circonscriptions):
    """Computes the total number of voters not represented by any deputy."""
    return [sum(c['t1'][i + 6] for c in circonscriptions.values() if c['winner'] != n)
            for i, n in enumerate(NUANCES)]


def hare(proportions, winners, nb_prop):
    """Given a set of proportions, a set of deputies already elected, and
    a number of deputies to add, computes the additional deputies and
    adds them to the winner set, using the Hare rule."""
    reminders = [nb_prop * prop - int(nb_prop * prop) for prop in proportions]
    for n in argtopk(reminders, nb_prop - sum(winners)):
        winners[n] += 1
    assert sum(winners) == nb_prop
    return(winners)


def dhondt(proportions, winners, nb_prop):
    """Given a set of proportions, a set of deputies already elected, and
    a number of deputies to add, computes the additional deputies and
    adds them to the winner set, using the d'Hondt rule."""
    for _ in range(nb_prop - sum(winners)):
        averages = [prop / (w + 1) for prop, w in zip(proportions, winners)]
        winners[argmax(averages)] += 1
    assert sum(winners) == nb_prop
    return(winners)


def proportionality_additive(circonscriptions, nb_prop, rep_threshold, majority_winners,
                             rounding_method=hare):
    """The simplest rule: mixed proportional / majoritarian using the additive rule."""
    winners = [0] * len(NUANCES)  # this list is for the number of proportional deputies per party
    if nb_prop == 0:
        return majority_winners

    # First we compute the number of votes per party
    votes = circonscriptions_to_votes(circonscriptions)
    exprimes = sum(votes)
    if rep_threshold > 0:
        # if there is a representativity threshold,
        # all the parties below this threshold are set to 0
        votes = [v if v / exprimes >= rep_threshold else 0 for v in votes]
        exprimes = sum(votes)

    # Then we compute the ratio of seats that each party is supposed to get
    proportions = [v / exprimes for v in votes]

    # The number of seats we give to each party corresponds to the integral rounding of this ratio
    winners = [int(nb_prop * prop) for prop in proportions]

    # Then we attribute the rest of the seats according to the chosen rounding method
    winners = rounding_method(proportions, winners, nb_prop)

    return [i + j for (i, j) in zip(majority_winners, winners)]


def proportionality_compensatory(circonscriptions, nb_prop, rep_threshold, majority_winners,
                                 rounding_method=hare):
    """Compensatory rule. Une description de la règle compensatoire est donnée dans le rapport
    TerraNova sur la réforme des législatives : « les partis qui ont obtenu, avec les sièges
    distribués dans les circonscriptions suivant la règle majoritaire, déjà plus que ce que
    la proportionnelle pure leur donnerait, ne prennent pas part à la distribution des sièges
    supplémentaires. Les autres partis se les partagent proportionnellement à leur déficit
    en voix. »"""
    nb_maj = sum(majority_winners)

    winners = [0] * len(NUANCES)  # this list is for the number of proportional deputies per party
    if nb_prop == 0:
        return majority_winners

    # First we compute the number of votes per party
    votes = circonscriptions_to_votes(circonscriptions)
    exprimes = sum(votes)

    # if there is a representativity threshold, all the parties below this threshold are set to 0
    if rep_threshold > 0:
        votes = [v if v / exprimes >= rep_threshold else 0 for v in votes]
        exprimes = sum(votes)

    # Then we compute the number of deputies that should be attributed to each party
    # if the method was fully proportional
    full_prop_ratios = [v / exprimes for v in votes]

    # Then we compute the ratio of seats that each party is supposed to get
    # this ratio is proportional to the voting deficits, that is, the difference
    # between the nb of seats that the party should get with proportionality, and
    # the number of seats already obtained by majority. We exclude the parties
    # for whom this difference is negative (as they already reach their ratio)
    proportions = [max(0, pr - mw / nb_maj) for (pr, mw) in zip(full_prop_ratios, majority_winners)]

    # We normalize...
    current_sum = sum(proportions)
    proportions = [p * nb_prop / current_sum for p in proportions]
    assert sum(proportions) >= nb_prop - 0.1 and sum(proportions) <= nb_prop + 0.1, sum(proportions)

    # The number of seats we give to each party corresponds to the integral rounding of this ratio
    winners = [int(prop) for prop in proportions]

    # Then we attribute the rest of the seats according to the chosen rounding method
    winners = rounding_method(proportions, winners, nb_prop)

    return [i + j for (i, j) in zip(majority_winners, winners)]


def proportionality_corrective(circonscriptions, nb_prop, rep_threshold, majority_winners,
                               rounding_method=hare):
    """Corrective rule.
    « tous les partis participent à la distribution des sièges supplémentaires, mais le score
    de chaque parti est défalqué du nombre de voix obtenues par les candidats de ce parti qui
    sont directement élus dans les circonscriptions au scrutin majoritaire. »"""
    winners = [0] * len(NUANCES)  # this list is for the number of proportional deputies per party
    if nb_prop == 0:
        return majority_winners

    # First we compute the number of votes per party
    votes = circonscriptions_to_votes(circonscriptions)
    deficits = circonscriptions_to_deficits(circonscriptions)
    exprimes = sum(votes)
    nb_deficits = sum(deficits)

    # if there is a representativity threshold, all the parties below this threshold are set to 0
    if rep_threshold > 0:
        votes = [v if v / exprimes >= rep_threshold else 0 for v in votes]
        deficits = [d if v != 0 else 0 for d, v in zip(deficits, votes)]
        exprimes = sum(votes)
        nb_deficits = sum(deficits)

    # Then we compute the ratio of seats that each party is supposed to get
    # this ratio is proportional to the voting deficits
    proportions = [d / nb_deficits for d in deficits]

    # The number of seats we give to each party corresponds to the integral rounding of this ratio
    winners = [int(nb_prop * prop) for prop in proportions]

    # Then we attribute the rest of the seats according to the chosen rounding method
    winners = rounding_method(proportions, winners, nb_prop)

    return [i + j for (i, j) in zip(majority_winners, winners)]


def proportionality_per_district(circonscriptions, prime_majoritaire, rep_threshold):
    for circ in circonscriptions.values():
        circ_votes = [circ['t1'][n + 6] for n in range(len(NUANCES))]
#        circ_votes = [circ['t2'][n + 6] for n in range(len(NUANCES))]
#        if circ_votes[0] is None:
#            circ_votes = [circ['t1'][n + 6] for n in range(len(NUANCES))]
        exprimes = sum(circ_votes)
        if rep_threshold > 0:
            # if there is a representativity threshold,
            # all the parties below this threshold are set to 0
            circ_votes = [v if v / exprimes >= rep_threshold else 0 for v in circ_votes]
            exprimes = sum(circ_votes)

        # Then we compute the ratio of seats that each party is supposed to get
        circ["proportions"] = [v / exprimes for v in circ_votes]
        nb_deputes_maj = int(prime_majoritaire * circ["nb_deputes"])
        circ["nb_deputes_prop"] = circ["nb_deputes"] - nb_deputes_maj
        circ["winners_hare"] = [int(circ["nb_deputes_prop"] * prop)
                                for prop in circ["proportions"]]
        circ["winners_dhondt"] = [int(circ["nb_deputes_prop"] * prop)
                                  for prop in circ["proportions"]]
        hare(circ["proportions"], circ["winners_hare"], circ["nb_deputes_prop"])
        if "winner" in circ:
            winner_index = NUANCES.index(circ["winner"])
        else:
            winner_index = circ_votes.index(max(circ_votes))
        circ["winners_hare"][winner_index] += nb_deputes_maj
        dhondt(circ["proportions"], circ["winners_dhondt"], circ["nb_deputes_prop"])
        circ["winners_dhondt"][winner_index] += nb_deputes_maj
    return circonscriptions
