try:  # are we in a module?
    __file__
except NameError:  # if not, change path to root
    import os
    try:
        CWD
    except NameError:
        CWD = os.getcwd()
    os.chdir(os.path.join(CWD, ".."))

from constants import NUANCES, RESULTATS_OFFICIELS


def senate(circonscriptions, proportional_dpts_numbers, proportionality_method, plurality_method):
    """Ce type de scrutin mixe scrutin proportionnel et scrutin majoritaire sur une base géographique.
    Dans le cas des législatives, un certain nombre de départements sont choisis selon un certain
    critère (départements les plus peuplés par exemple). Dans ces départements, les députés sont
    élus à la proportionnelle. Pour l'ensemble des autres départements, c'est le scrutin majoritaire
    qui est utilisé."""
    # First we compute the set of circonscriptions for each proportional department
    prop_dpts = []
    for number in proportional_dpts_numbers:
        prop_dpts.append({code: c for code, c in circonscriptions.items()
                          if c["code_dpt"] == number})

    # Then we compute, for each concerned departments, the number of representative that
    # are elected using the proportional method in this department
    prop_winners = [0] * len(NUANCES)
    for prop_dpt in prop_dpts:
        prop_winners = [w1 + w2
                        for w1, w2 in zip(prop_winners,
                                          proportionality_method(prop_dpt, len(prop_dpt)))]

    # Finally, we compute the set of circonscriptions for all the non-proportional circonscriptions
    # and compute the majority winners for these circonscriptions
    maj_circonscriptions = {code: c
                            for code, c in circonscriptions.items()
                            if c["code_dpt"] not in proportional_dpts_numbers}
    _, maj_winners = plurality_method(maj_circonscriptions)
    maj_nb_winners = [len([c for c, w in maj_winners.items() if w == n]) for n in NUANCES]
    return [w1 + w2 for w1, w2 in zip(prop_winners, maj_nb_winners)]


def test_with_no_proportional():
    from data.data_loader import load_circonscriptions
    from rules.proportional import proportionality_additive, dhondt

    circonscriptions = load_circonscriptions()
    for expected, actual in zip(senate(circonscriptions, [],
                                       lambda circ, nb: proportionality_additive(circ, nb,
                                                                                 0, dhondt),
                                       lambda circ: ({}, {
                                           code: c["winner"] for code, c in circ.items()})),
                                RESULTATS_OFFICIELS):
        assert expected == actual
