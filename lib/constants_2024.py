NUANCES = [
    'EXG', # Extrême gauche
    'COM', # Parti communiste français
    'FI', # La France insoumise
    'SOC', # Parti socialiste
    'RDG', # Parti radical de gauche
    'VEC', # Les Ecologistes
    'DVG', # Divers gauche
    'UG', # Union de la gauche
    'ECO', # Ecologistes
    'REG', # Régionaliste
    'DIV', # Divers
    'REN', # Renaissance
    'MDM', # Modem
    'HOR', # Horizons
    'ENS', # Ensemble ! (Majorité présidentielle)
    'DVC', # Divers centre
    'UDI', # Union des Démocrates et Indépendants
    'LR', # Les Républicains
    'DVD', # Divers droite
    'DSV', # Droite souverainiste
    'RN', # Rassemblement National
    'REC', # Reconquête !
    'UXD', # Union de l'extrême droite
    'EXD', # Extrême droite
]

COLORS = {
    "EXG": 0x91070f,
    "COM": 0x91070f,
    "FI":  0xff3333,
    "SOC": 0xff7999,
    'RDG': 0xb94e97,
    'VEC': 0x92bc1e,
    'DVG': 0xda679e,
    'UG': 0xa11870,
    'ECO': 0x92bc1e,
    'REG': 0x78668d,
    'DIV': 0x957cb6,
    'REN': 0xe5c047,
    'MDM': 0xe5c047,
    'HOR': 0xe5c047,
    'ENS': 0xff9f0e,
    'DVC': 0xe1b000,
    'UDI': 0xe1b000,
    'LR': 0x0890c5,
    'DVD': 0x6387a8,
    'DSV': 0x443859,
    'RN': 0x8d6026,
    'REC': 0x7f6659,
    'UXD': 0x505455,
    'EXD': 0x505455,
}

RESULTATS_OFFICIELS = [
    0, 0, 0, 2, 0, 0, 12, 178, 1, 9, 1, 0, 0,
    6, 150, 6, 3, 39, 27, 0, 125, 0, 17, 1]

N17 = 577
N17_T2 = 501
N17_CANTONS = None
N17_CANTONS_T2 = None
N22 = 404

