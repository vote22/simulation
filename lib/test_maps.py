#! /usr/bin/env python3

import sys
from utils.maps import test_draw_map
from seagull.xml.serializer import serialize

scene = test_draw_map(cantons=True)
sys.stdout.write(serialize(scene))
