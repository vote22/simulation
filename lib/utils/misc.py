def argmax(tab):
    return argtopk(tab, 1)[0]


def argtopk(tab, k):
    return sorted(range(len(tab)), key=lambda i: -tab[i])[:k]


def rgb(h):
    h = '%06x' % h
    return tuple(int(u, 16)/255 for u in (h[:2], h[2:4], h[4:]))
