try:  # are we in a module?
    __file__
except NameError:  # if not, change path to root
    import os
    try:
        CWD
    except NameError:
        CWD = os.getcwd()
    os.chdir(os.path.join(CWD, "../.."))

from seagull import scenegraph as sg
from seagull.xml.serializer import serialize

from math import log, cos, sin, pi, sqrt
from collections import defaultdict

from utils.misc import rgb
from constants import NUANCES, COLORS


def representativity_graph_cartesian(winners, votes, M=None, ratio=0.5, k=None):
    W = 1000
    # W = horizontal scale (= width of the graph from left to right)
    H = int(1000*ratio)
    # H = vertical scale (= height of the highest bar)
    scene = sg.Group(transform=[sg.Scale(1, -1)])

    x = 0

    N = sum(winners)
    V = sum(votes)/N
    if M is None:
        M = max(v / n if n != 0 else 0 for v, n in zip(votes, winners))
    if k is not None:
        M = k*V

    for i, nuance in enumerate(NUANCES):
        n = winners[i]
        if n == 0:
            continue
        v = votes[i]/n
        scene.children += [
            sg.Rectangle(
                x=x, width=n*W/N, y=0, height=v*H/M,
                title="%s (×%.2f)" % (nuance, v/V),
                fill=sg.Color(*rgb(COLORS[nuance]))
            )
        ]
        x += n*W/N
    scene.children += [
        sg.Line(
            x2=x, y1=V*2**k*H/M, y2=V*2**k*H/M,
            stroke_width=1 if k == 0 else .5,
            stroke=sg.Color.black
        )
        for k in range(int(log(M/V, 2)) - 4, int(log(M/V, 2)) + 1)
    ] + [
        sg.Text(
            "%i" % (V*2**k,),
            text_anchor="end",
            transform=[sg.Translate(-5, V*2**k*H/M-5), sg.Scale(1, -1)]
        )
        for k in range(int(log(M/V, 2)) - 4, int(log(M/V, 2)) + 1)
    ]

    return scene


def representativity_graph_polar(winners, votes, k=None):
    R0, R1 = 100, 600

    N = sum(winners)
    V = sum(votes)/N
    if k is not None:
        M = k*V
        bg = [sg.Rectangle(x=-k*3*R1, width=2*3*k*R1, y=-40, height=3*k*R1+40, fill=None,
                           stroke=sg.Color.black)]
        ks = range(-2, 3)
        R_max = 3*k*R1
    else:
        M = max(v / n if n != 0 else 0 for v, n in zip(votes, winners))
        bg = []
        ks = range(int(log(M/V, 2)) - 4, int(log(M/V, 2)) + 1)
        R_max = float('inf')

    V1 = M

    scene = sg.Group(transform=[sg.Scale(1, -1)], children=bg)
    a = pi
    for i, nuance in enumerate(NUANCES):
        n = winners[i]
        if n == 0:
            continue
        v = votes[i]/n
        r = sqrt(v/V1*(R1**2-R0**2) + R0**2)
        if r > R_max:
            continue
        da = pi*n/N
        ca0, sa0 = cos(a), sin(a)
        a -= da
        ca1, sa1 = cos(a), sin(a)
        scene.children += [
            sg.Path(
                fill=sg.Color(*rgb(COLORS[nuance])),
                title="%s (×%.2f)" % (nuance, v/V),
                d=[
                    "M", (r*ca0, r*sa0),
                    "A", (r, r), 0, (0, 0), (r*ca1, r*sa1),
                    "L", (R0*ca1, R0*sa1),
                    "A", (R0, R0), 0, (0, 1), (R0*ca0, R0*sa0),
                    "Z"
                ]
            )
        ]

    marks = sg.Group()
    for k in ks:
        vk = V*2**k
        r = sqrt(vk/V1*(R1**2-R0**2) + R0**2)
        ca, sa = -1, 0
        marks.children += [
            sg.Text(
                "%i" % (vk,),
                text_anchor="end",
                transform=[sg.Translate(-r, -20), sg.Scale(1, -1)]
            ),
        ]

        marks.children += [
            sg.Path(
                stroke=sg.Color.black,
                fill=None,
                stroke_width=3 if k == 0 else 1,
                d=[
                    "M", (r*ca, r*sa),
                    "A", (r, r), 0, (0, 0), (r, 0),
                ]
            ),
            sg.Text(
                "%i" % (vk,),
                transform=[sg.Translate(r, -20), sg.Scale(1, -1)]
            ),
        ]

    scene.children.append(marks)

    return scene


def draw_cartesian(settings):
    scene = sg.Group()

    current_y = 0
    for (winners, votes, label) in settings:
        current_graph = sg.Group(transform=[sg.Translate(0, current_y)])
        current_graph.children += [representativity_graph_cartesian(winners, votes, ratio=0.25),
                                   sg.Text(label,
                                           text_anchor="middle",
                                           transform=[sg.Translate(500, 20)])]
        scene.children += [current_graph]
        current_y += 300
    return scene


def draw_polar(settings):
    scene = sg.Group()

    current_y = 0
    for (winners, votes, label) in settings:
        current_graph = sg.Group(transform=[sg.Translate(0, current_y)])
        current_graph.children += [representativity_graph_polar(winners, votes),
                                   sg.Text(label,
                                           text_anchor="middle",
                                           transform=[sg.Translate(0, 50)])]
        scene.children += [current_graph]
        current_y += 650

    return scene


def export_cartesian(settings, basename="cartesian"):
    for i, s in enumerate(settings):
        winners, votes, label = s
        with open("%s-%s.svg" % (basename, format(i + 1, "0>2")), "w") as file:
            current_graph = sg.Group()
            current_graph.children += [representativity_graph_cartesian(winners, votes, k=2)]
            file.write(serialize(current_graph))


def export_polar(settings, basename="polar"):
    for i, s in enumerate(settings):
        winners, votes, label = s
        with open("%s-%s.svg" % (basename, format(i + 1, "0>2")), "w") as file:
            current_graph = sg.Group()
            current_graph.children += [representativity_graph_polar(winners, votes, k=2)]
            file.write(serialize(current_graph))


def histogram(values, bucket_len, h):
    buckets = defaultdict(int)
    for e in values:
        buckets[e//bucket_len*bucket_len] += 1

    return sg.Group(
        transform=[sg.Scale(1/bucket_len, -h)],
        children=[
            sg.Rectangle(x=k, width=bucket_len, height=buckets[k])
            for k in sorted(buckets)
        ]
    )
