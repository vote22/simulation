try:  # are we in a module?
    __file__
except NameError:  # if not, change path to root
    import os
    try:
        CWD
    except NameError:
        CWD = os.getcwd()
    os.chdir(os.path.join(CWD, ".."))


def quality(winners, votes):
    """The q-index represents the normalized sum over all parties of the absolute
    difference between the proportion of deputies this party has received and
    the proportion it should have received in a fully proportional parliament."""
    N = sum(winners)
    V = sum(votes)
    aerror = 0.
    for n, v in zip(winners, votes):
        aerror += abs(v/V-n/N)
    return 1-aerror/2
