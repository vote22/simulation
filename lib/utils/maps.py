try:  # are we in a module?
    __file__
except NameError:  # if not, change path to root
    import os
    try:
        CWD
    except NameError:
        CWD = os.getcwd()
    os.chdir(os.path.join(CWD, "../.."))

from seagull import scenegraph as sg

from utils.misc import rgb
from constants import COLORS

from pyproj import Transformer
from shapely.geometry import shape

RGF93_TRANSFORMER = Transformer.from_crs(4326, 2154, always_xy=True)


def _RGF93(x, y):
    nx, ny = RGF93_TRANSFORMER.transform(x, y)
    return nx / 1000, ny / 1000


def _polygon(rings, projection=_RGF93):
    d = []
    for ring in rings:
        r = ["M"]
        for x, y in ring:
            r += [projection(x, y), "L"]
        r[-1] = "Z"
        d += r
    return sg.Path(d=d)


def _point(x, y, r, projection=_RGF93):
    nx, ny = projection(x, y)
    return sg.Circle(cx=nx, cy=ny, r=r)


def _line(x1, y1, x2, y2, projection=_RGF93):
    nx1, ny1 = projection(x1, y1)
    nx2, ny2 = projection(x2, y2)
    return sg.Line(x1=nx1, x2=nx2, y1=ny1, y2=ny2)


def _add_departements(f):
    def wrapper(*args, draw_dep=False):
        from data.data_loader import load_departement_geometries
        departement_geometries = load_departement_geometries()

        scene = f(*args)

        if not draw_dep:
            return scene

        for feature in departement_geometries['features']:
            geometry = feature['geometry']
            assert geometry['type'] in ['Polygon', 'MultiPolygon']
            if geometry['type'] == 'Polygon':
                g = _polygon(geometry["coordinates"])
            elif geometry['type'] == 'MultiPolygon':
                g = sg.Group(children=[
                    _polygon(p)
                    for p in geometry["coordinates"]
                ])
            g.fill = None
            g.stroke = sg.Color.black
            g.stroke_width = 1
            g.opacity = 0.7
            scene.children += [g]

        return scene
    return wrapper


@_add_departements
def draw_map(geometries, exclude, color):
    """This function draws a map of electoral districts.
    - geometries: the list of polygon representing the districts
    - exclude: the list of polygon IDs to exclude (useful to exclude DOM/TOM in France)
    - color: a function mapping every ID to a color."""
    scene = sg.Group(transform=[sg.Scale(1, -1)])

    for feature in geometries['features']:
        if exclude(feature['properties']['ID']):
            continue
        geometry = feature['geometry']
        assert geometry['type'] in ['Polygon', 'MultiPolygon']
        if geometry['type'] == 'Polygon':
            g = _polygon(geometry["coordinates"])
        elif geometry['type'] == 'MultiPolygon':
            g = sg.Group(children=[
                _polygon(p)
                for p in geometry["coordinates"]
            ])
        g.fill = color(feature['properties']['ID'])
        scene.children += [g]

    return scene


@_add_departements
def draw_map_and_graph(geometries, edges, exclude, color):
    """This function draws a map of electoral districts, with connexity edges.
    - geometries: the list of polygon representing the districts
    - edges: the connexity edges
    - exclude: the list of polygon IDs to exclude (useful to exclude DOM/TOM in France)
    - color: a function mapping every ID to a color."""
    scene = sg.Group(transform=[sg.Scale(1, -1)])

    for feature in geometries['features']:
        if exclude(feature['properties']['ID']):
            continue
        geometry = feature['geometry']
        assert geometry['type'] in ['Polygon', 'MultiPolygon']
        if geometry['type'] == 'Polygon':
            g = _polygon(geometry["coordinates"])
        elif geometry['type'] == 'MultiPolygon':
            g = sg.Group(children=[
                _polygon(p)
                for p in geometry["coordinates"]
            ])
        g.fill = color(feature['properties']['ID'])
        g.stroke = sg.Color.black
        g.stroke_width = 1
        g.opacity = 0.3
        scene.children += [g]

    for i, feature in enumerate(geometries['features']):
        if exclude(feature['properties']['ID']):
            continue
        geometry = feature['geometry']
        centroid = shape(feature['geometry']).centroid
        for j in range(i + 1, len(geometries['features'])):
            ID2 = geometries['features'][j]["properties"]["ID"]
            if ID2 in edges[feature['properties']['ID']]:
                centroid2 = shape(geometries['features'][j]['geometry']).centroid
                g3 = _line(centroid.x, centroid.y, centroid2.x, centroid2.y)
                g3.stroke_width = 1
                g3.stroke = sg.Color.black
                scene.children += [g3]
        g2 = _point(centroid.x, centroid.y, 3)
        g2.fill = color(feature['properties']['ID'])
        g2.stroke = sg.Color.black
        g2.stroke_width = 1
        scene.children += [g2]

    return scene


def test_draw_map(cantons=False):
    from data.data_loader import (
        load_circonscriptions,
        load_cantons,
        load_circonscription_geometries,
        load_canton_geometries
    )

    districts = load_cantons() if cantons else load_circonscriptions()
    district_geometries = load_canton_geometries() if cantons else load_circonscription_geometries()

    return draw_map(district_geometries,
                    lambda id: id.startswith('Z'),
                    lambda id: sg.Color(*rgb(COLORS[districts[id]['winner']])),
                    draw_dep=True)
