NUANCES = [
    "EXG", "COM", "FI",  "SOC", "RDG", "DVG", "ECO", "DIV", "REG",
    "REM", "MDM", "UDI", "LR",  "DVD", "DLF", "FN",  "EXD",
]

(
    EXG, COM, FI,  SOC, RDG, DVG, ECO, DIV, REG,
    REM, MDM, UDI, LR,  DVD, DLF, FN,  EXD
) = NUANCES

COLORS = {
    "EXG": 0x91070f,
    "COM": 0x91070f,
    "FI":  0xc84529,
    "SOC": 0xe4758f,
    "RDG": 0xb94e97,
    "DVG": 0xfadceb,
    "ECO": 0x92bc1e,
    "DIV": 0xcccccc,
    "REG": 0xede8c9,
    "REM": 0xe5c047,
    "MDM": 0xe5c047,
    "UDI": 0xb3daf2,
    "LR":  0x478cb2,
    "DVD": 0xa4becd,
    "DLF": 0x443859,
    "FN":  0x7f6659,
    "EXD": 0x505455
}

# Données officielles du Ministère de l'Intérieur ###

RESULTATS_OFFICIELS = [
    0, 10, 17, 30, 3, 12, 1, 3, 5,
    308, 42, 18, 112, 6, 1, 8, 1]

N17 = 577
N17_T2 = 573
N17_CANTONS = 2090
N17_CANTONS_T2 = 2082
N22 = 404
