#! /usr/bin/env python3

from data.data_loader import load_circonscriptions
from rules.proportional import (
    circonscriptions_to_votes,
    proportionality_additive, proportionality_compensatory, proportionality_corrective,
    dhondt, hare
)
from constants import RESULTATS_OFFICIELS, NUANCES
from utils.viz import export_cartesian, export_polar
from utils.metrics import quality


# Additive rule

circonscriptions = load_circonscriptions()
votes = circonscriptions_to_votes(circonscriptions)
settings = []

for i, ratio_prop in enumerate([0, 0.1, 0.15, 0.2, 0.3]):
    winners = proportionality_additive(circonscriptions,
                                       int(ratio_prop / (1 - ratio_prop) * len(circonscriptions)),
                                       0.00,
                                       RESULTATS_OFFICIELS,
                                       dhondt)
    assert sum(winners) == (
        sum(RESULTATS_OFFICIELS) + int(ratio_prop / (1 - ratio_prop) * len(circonscriptions)))
    settings.append((winners, votes, "Taux de proportionalité : %d %%" % (100 * ratio_prop)))

if True:
    export_cartesian(settings, "cartesian-additive")
    export_polar(settings, "polar-additive")
    for winners, votes, label in settings:
        print("{} → {:.2f}".format(label, quality(winners, votes)))


votes = circonscriptions_to_votes(circonscriptions)

for i, ratio_prop in enumerate([0, 0.1, 0.15, 0.2, 0.3]):
    winners_hare = proportionality_additive(
        circonscriptions,
        int(ratio_prop / (1 - ratio_prop) * len(circonscriptions)),
        0.0,
        RESULTATS_OFFICIELS,
        hare)
    assert sum(winners_hare) == (
        sum(RESULTATS_OFFICIELS) + int(ratio_prop / (1 - ratio_prop) * len(circonscriptions)))
    winners_dhondt = proportionality_additive(
        circonscriptions,
        int(ratio_prop / (1 - ratio_prop) * len(circonscriptions)),
        0.0,
        RESULTATS_OFFICIELS,
        dhondt)
    assert sum(winners_dhondt) == (
        sum(RESULTATS_OFFICIELS) + int(ratio_prop / (1 - ratio_prop) * len(circonscriptions)))
    print([w1 - w2 for w1, w2 in zip(winners_hare, winners_dhondt)],
          sum(abs(w1 - w2) for w1, w2 in zip(winners_hare, winners_dhondt)) / len(NUANCES))

# Règle compensatoire

votes = circonscriptions_to_votes(circonscriptions)
settings = []

for i, ratio_prop in enumerate([0, 0.1, 0.15, 0.2, 0.3]):
    winners = proportionality_compensatory(
        circonscriptions,
        int(ratio_prop / (1 - ratio_prop) * len(circonscriptions)),
        0.0,
        RESULTATS_OFFICIELS,
        dhondt)
    assert sum(winners) == (
        sum(RESULTATS_OFFICIELS) + int(ratio_prop / (1 - ratio_prop) * len(circonscriptions)))
    settings.append((winners, votes, "Taux de proportionalité : %d %%" % (100 * ratio_prop)))

if True:
    export_cartesian(settings, "cartesian-compensatoire")
    export_polar(settings, "polar-compensatoire")
    for winners, votes, label in settings:
        print("{} → {:.2f}".format(label, quality(winners, votes)))

votes = circonscriptions_to_votes(circonscriptions)

for i, ratio_prop in enumerate([0, 0.1, 0.15, 0.2, 0.3]):
    winners_hare = proportionality_compensatory(
        circonscriptions,
        int(ratio_prop / (1 - ratio_prop) * len(circonscriptions)),
        0.05,
        RESULTATS_OFFICIELS,
        hare)
    assert sum(winners_hare) == (
        sum(RESULTATS_OFFICIELS) + int(ratio_prop / (1 - ratio_prop) * len(circonscriptions)))
    winners_dhondt = proportionality_compensatory(
        circonscriptions,
        int(ratio_prop / (1 - ratio_prop) * len(circonscriptions)),
        0.05,
        RESULTATS_OFFICIELS,
        dhondt)
    assert sum(winners_dhondt) == (
        sum(RESULTATS_OFFICIELS) + int(ratio_prop / (1 - ratio_prop) * len(circonscriptions)))
    print([w1 - w2 for w1, w2 in zip(winners_hare, winners_dhondt)],
          sum(abs(w1 - w2) for w1, w2 in zip(winners_hare, winners_dhondt)) / len(NUANCES))

# Règle corrective

votes = circonscriptions_to_votes(circonscriptions)
settings = []

for i, ratio_prop in enumerate([0, 0.1, 0.15, 0.2, 0.3]):
    winners = proportionality_corrective(circonscriptions,
                                         int(ratio_prop / (1 - ratio_prop) * len(circonscriptions)),
                                         0.0,
                                         RESULTATS_OFFICIELS,
                                         dhondt)
    assert sum(winners) == (
        sum(RESULTATS_OFFICIELS) + int(ratio_prop / (1 - ratio_prop) * len(circonscriptions)))
    settings.append((winners, votes, "Taux de proportionalité : %d %%" % (100 * ratio_prop)))

if True:
    export_cartesian(settings, "cartesian-corrective")
    export_polar(settings, "polar-corrective")
    for winners, votes, label in settings:
        print("{} → {:.2f}".format(label, quality(winners, votes)))

votes = circonscriptions_to_votes(circonscriptions)

for i, ratio_prop in enumerate([0, 0.1, 0.15, 0.2, 0.3]):
    winners_hare = proportionality_corrective(
        circonscriptions,
        int(ratio_prop / (1 - ratio_prop) * len(circonscriptions)),
        0.05,
        RESULTATS_OFFICIELS,
        hare)
    assert sum(winners_hare) == (
        sum(RESULTATS_OFFICIELS) + int(ratio_prop / (1 - ratio_prop) * len(circonscriptions)))
    winners_dhondt = proportionality_corrective(
        circonscriptions,
        int(ratio_prop / (1 - ratio_prop) * len(circonscriptions)),
        0.05,
        RESULTATS_OFFICIELS,
        dhondt)
    assert sum(winners_dhondt) == (
        sum(RESULTATS_OFFICIELS) + int(ratio_prop / (1 - ratio_prop) * len(circonscriptions)))
    print([w1 - w2 for w1, w2 in zip(winners_hare, winners_dhondt)],
          sum(abs(w1 - w2) for w1, w2 in zip(winners_hare, winners_dhondt)) / len(NUANCES))
