try:  # are we in a module?
    __file__
except NameError:  # if not, change path to root
    import os
    try:
        CWD
    except NameError:
        CWD = os.getcwd()
    os.chdir(os.path.join(CWD, "../.."))

from utils.misc import rgb

from random import sample, choice
from collections import defaultdict

from data.data_loader import (
    load_circonscriptions,
    load_circonscription_edges,
    load_circonscription_geometries,
    load_cantons,
    load_canton_edges,
    load_canton_geometries
)


def merge(*circo, circ_kw="circo", nb_merged=lambda circo: len(circo)):  # Merges k circonscriptions
    return {
        "code_dpt": circo[0]["code_dpt"],
        "dpt": circo[0]["dpt"],
        circ_kw: "+".join(c[circ_kw] for c in circo),
        "t1": [sum(circo[k]["t1"][nuance] for k in range(len(circo)))
               for nuance in range(len(circo[0]["t1"]))],
        "t2": [None] * len(circo[0]["t2"]),
        "nb_merged": nb_merged(circo)
    }


def redistrict_merge_by_function(merge_key,
                                 nb_merged,
                                 input_dist=load_circonscriptions()):
    equiv_classes = defaultdict(list)
    for circo in input_dist.values():
        equiv_classes[merge_key(circo)] += [circo]
    return {key: merge(*circos, nb_merged=nb_merged) for key, circos in equiv_classes.items()}


def redistrict_merge(target, input_dist=load_circonscriptions(),
                     circ_kw="circo", exclude=lambda id: id.startswith('ZZ')):
    if exclude:
        new_input_dist = {c: circ
                          for c, circ in input_dist.items()
                          if not exclude(c)}
        result = redistrict_merge(target - len(input_dist) + len(new_input_dist),
                                  new_input_dist, circ_kw, None)
        result.update({c: circ
                       for c, circ in input_dist.items()
                       if exclude(c)})
        return result
    # First we compute the number of circonscription per departement
    departements = {}
    for k, v in input_dist.items():
        if v["code_dpt"] not in departements:
            departements[v["code_dpt"]] = {}
        departements[v["code_dpt"]][k] = v
    assert target >= len(departements),\
        "Number of target circonscriptions too small: {}".format(target)

    init_nb_circo = len(input_dist)
    nb_circo = {code_dpt: len(circos) for code_dpt, circos in departements.items()}
    target_nb_circo = {circo: nb_circo[circo] * target / init_nb_circo for circo in nb_circo}
    total_nb_circo = init_nb_circo

    # Since the target numbers of circos are not integral, a set of departements
    # will have to merge one more circo. These departements must be chosen only
    # among the set of departements whose target is greater than 1 (otherwise there
    # is a risk of ending up with a departement with no circo at all).
    # Reminder is the exact number of those departements needed.
    reminder = sum(int(target_nb_circo[circo] + 1) for circo in target_nb_circo) - target
    chosen_ones = [code_dpt for code_dpt in departements
                   if nb_circo[code_dpt] - target_nb_circo[code_dpt] > 0
                   and int(target_nb_circo[code_dpt] + 1) > 1]
    # Chosen ones is the set of all candidates departements. Among
    # them, we choose only a subset of (reminder) departements, as required.
    if len(chosen_ones) > reminder:
        chosen_ones = sample(chosen_ones, reminder)

    # Then we start to merge. For each departement, we do it in a single pass,
    # directly reaching the target number of circos.
    for code_dpt, circos in departements.items():
        candidate_circos = set(circos)
        # this set contains the circos still mergeable (initially all circos)
        nb_init = nb_circo[code_dpt]
        nb_final = int(target_nb_circo[code_dpt] + 1)
        if code_dpt in chosen_ones:
            nb_final -= 1
        k = nb_init // nb_final
        r = nb_init % nb_final
        # It means that r circos must contain exactly (k+1) former circos
        # and nb_final-r circos must contain exactly k former circos

        # These circonscriptions will result from the aggregation of (k+1) former circos
        for i in range(r):
            code_circos = sample(list(candidate_circos), k + 1)
            merged = merge(*(circos[c] for c in code_circos), circ_kw=circ_kw)
            for c in code_circos:
                del circos[c]
                candidate_circos.remove(c)
            circos["+".join(code_circos)] = merged
            nb_circo[code_dpt] -= k
            total_nb_circo -= k
        # These circonscriptions will result from the aggregation of k former circos
        for i in range(nb_final - r):
            code_circos = sample(list(candidate_circos), k)
            merged = merge(*(circos[c] for c in code_circos), circ_kw=circ_kw)
            for c in code_circos:
                del circos[c]
                candidate_circos.remove(c)
            circos["+".join(code_circos)] = merged
            nb_circo[code_dpt] -= (k - 1)
            total_nb_circo -= (k - 1)

    # Finally, it might be the case where there are still some circos to remove.
    # This is only the case when the number of depts in chosen_ones were too small
    # due to the fact that too much depts have a target number = 1.
    # In that case, we randomly choose depts among those that still have more
    # than 1 circo, and merge 2 random circos inside them.
    if total_nb_circo - target != 0:
        chosen_ones = sample([code_dpt for code_dpt in departements
                              if nb_circo[code_dpt] > 1], total_nb_circo - target)
        for code_dpt in chosen_ones:
            circos = departements[code_dpt]
            code_circo1, code_circo2 = sample(list(circos), 2)
            merged = merge(circos[code_circo1], circos[code_circo2], circ_kw=circ_kw)
            del circos[code_circo1]
            del circos[code_circo2]
            circos["{}+{}".format(code_circo1, code_circo2)] = merged
            nb_circo[code_dpt] -= 1
            total_nb_circo -= 1

    output_dist = {}
    for circos in departements.values():
        for c in circos:
            output_dist[c] = circos[c]
    return output_dist


def is_connected(edges, nodes):
    if not nodes:
        return True
    open_nodes = [next(iter(nodes))]
    marked_nodes = set(open_nodes)
    while open_nodes:
        current_node = open_nodes.pop()
        for n in edges[current_node]:
            if n in nodes and n not in marked_nodes:
                open_nodes.append(n)
                marked_nodes.add(n)
    return len(nodes) == len(marked_nodes)


def part_graph(edges, nb_parts):
    assert len(edges), "The initial graph should not be empty!"
    nb_init = len(edges)
    nb_final = nb_parts
    k = nb_init // nb_final
    r = nb_init % nb_final
    # It means that r clusters must contain exactly (k+1) nodes
    # and nb_final-r clusters must contain exactly k nodes

    candidate_edges = {}  # this set contains the nodes still to be clustered
    for c, e in edges.items():
        candidate_edges[c] = [node for node in e]

    classes = {}

    # These clusters will be formed of (k+1) nodes
    for i in range(nb_final):
        # The first r clusters will be formed of (k+1) nodes, while the
        # rest will be formed of k nodes
        nb_nodes = k + 1 if i < r else k
        # We store the current node candidates in open_nodes and closed_nodes
        # We start with the nodes with lower increasing degree
        assert candidate_edges
        # LIFO structure (breadth-first-search...)
        open_nodes = [sorted(candidate_edges, key=lambda code: len(edges[code]))[0]]
        marked_nodes = set(open_nodes)
        closed_nodes = set()
        skip_connection = False
        while len(closed_nodes) < nb_nodes:
            if not open_nodes:
                open_nodes = [(set(candidate_edges.keys()).difference(marked_nodes)).pop()]
                marked_nodes.add(open_nodes[0])
                skip_connection = True
            expanded_node = open_nodes.pop(0)
            complementary = set(candidate_edges.keys()).difference(closed_nodes)
            complementary.remove(expanded_node)
            if skip_connection or is_connected(candidate_edges, complementary):
                closed_nodes.add(expanded_node)
                for n in candidate_edges[expanded_node]:
                    if n not in marked_nodes:
                        open_nodes.append(n)
                        marked_nodes.add(n)
            else:
                marked_nodes.remove(expanded_node)
        assert len(closed_nodes) == nb_nodes

        # closed_nodes now contains the right number of nodes.
        # We can attribute the current cluster number and delete
        # These nodes from the list.
        for code in closed_nodes:
            classes[code] = i
            for c, e in candidate_edges.items():
                if code in e:
                    e.remove(code)
            candidate_edges.pop(code)

    return classes


def redistrict_merge_connected(target, input_dist=load_circonscriptions(),
                               input_edges=load_circonscription_edges(),
                               circ_kw="circo", exclude=lambda id: id.startswith('ZZ')):
    if exclude:
        new_input_dist = {c: circ
                          for c, circ in input_dist.items()
                          if not exclude(c)}
        new_input_edges = {c: [e for e in input_edges[c] if not exclude(e)]
                           for c in input_edges
                           if not exclude(c)}
        result = redistrict_merge_connected(target - len(input_dist) + len(new_input_dist),
                                            new_input_dist,
                                            new_input_edges,
                                            circ_kw, None)
        result.update({c: circ
                       for c, circ in input_dist.items()
                       if exclude(c)})
        return result

    # First we compute the number of circonscription per departement
    departements = {}
    for k, v in input_dist.items():
        if v["code_dpt"] not in departements:
            departements[v["code_dpt"]] = {}
        departements[v["code_dpt"]][k] = v
    assert target >= len(departements), f"Number of target circonscriptions too small: {target}"

    init_nb_circo = len(input_dist)
    nb_circo = {code_dpt: len(circos) for code_dpt, circos in departements.items()}
    target_nb_circo = {circo: nb_circo[circo] * target / init_nb_circo for circo in nb_circo}
    total_nb_circo = init_nb_circo

    # Since the target numbers of circos are not integral, a set of departements
    # will have to merge one more circo. These departements must be chosen only
    # among the set of departements whose target is greater than 1 (otherwise there
    # is a risk of ending up with a departement with no circo at all).
    # Reminder is the exact number of those departements needed.
    reminder = sum(int(target_nb_circo[circo] + 1) for circo in target_nb_circo) - target
    chosen_ones = [code_dpt for code_dpt in departements
                   if nb_circo[code_dpt] - target_nb_circo[code_dpt] > 0
                   and int(target_nb_circo[code_dpt] + 1) > 1]
    # Chosen ones is the set of all candidates departements. Among
    # them, we choose only a subset of (reminder) departements, as required.
    if len(chosen_ones) > reminder:
        chosen_ones = sample(chosen_ones, reminder)

    # Then we start to merge. For each departement, we do it in a single pass,
    # directly reaching the target number of circos.
    for code_dpt, circos in departements.items():
        # First, we extract the graph for the department concerned
        current_edges = {}
        for code, neighbours in input_edges.items():
            if code in circos:
                current_edges[code] = []
                for n in neighbours:
                    if n in circos:
                        current_edges[code].append(n)
        if not current_edges:
            # It might happen that some dpts are not present in edges (e.g. Français de l'étranger)
            # print("%s non présent dans le graphe" % code_dpt)
            current_edges = {code: [] for code in circos}

        nb_final = int(target_nb_circo[code_dpt] + 1)
        if code_dpt in chosen_ones:
            nb_final -= 1

        # Then, we partition the graph in nb_final clusters
        classes = part_graph(current_edges, nb_final)

        # Finally, we merge the concerned circos
        for i in range(nb_final):
            code_circos = [c for c in classes if classes[c] == i]
            merged = merge(*(circos[c] for c in code_circos), circ_kw=circ_kw)
            for c in code_circos:
                del circos[c]
            circos["+".join(code_circos)] = merged
            nb_circo[code_dpt] -= (len(code_circos) - 1)
            total_nb_circo -= (len(code_circos) - 1)

    # Finally, it might be the case where there are still some circos to remove.
    # This is only the case when the number of depts in chosen_ones were too small
    # due to the fact that too much depts have a target number = 1.
    # In that case, we randomly choose depts among those that still have more
    # than 1 circo, and merge 2 random circos inside them.
    if total_nb_circo - target != 0:
        chosen_ones = sample([code_dpt for code_dpt in departements
                              if nb_circo[code_dpt] > 1], total_nb_circo - target)
        for code_dpt in chosen_ones:
            circos = departements[code_dpt]
            code_circo1, code_circo2 = sample(list(circos), 2)
            merged = merge(circos[code_circo1], circos[code_circo2], circ_kw=circ_kw)
            del circos[code_circo1]
            del circos[code_circo2]
            circos["{}+{}".format(code_circo1, code_circo2)] = merged
            nb_circo[code_dpt] -= 1
            total_nb_circo -= 1

    output_dist = {}
    for circos in departements.values():
        for c in circos:
            output_dist[c] = circos[c]
    return output_dist


def draw_test_part_graph():
    from utils.maps import draw_map_and_graph
    from seagull import scenegraph as sg

    dpts = ['69', '38', '01', '73', '74', '26', '07', '42']
    # dpts = ['31', '81', '11', '09', '65']

    circonscription_edges = load_circonscription_edges()
    ra_edges = {}
    for code, neighbours in circonscription_edges.items():
        if code[:2] in dpts:
            ra_edges[code] = []
            for n in neighbours:
                if n[:2] in dpts:
                    ra_edges[code].append(n)

    NB_CLASSES = 3
    classes = part_graph(ra_edges, NB_CLASSES)
#    for k in range(NB_CLASSES):
#        print("La classe {} {}".format(k, "est connectée"
#                                       if is_connected(ra_edges,
#                                                       [c for c in ra_edges if classes[c] == k])
#                                       else "n'est pas connectée"))

    palette = [0xe41a1c, 0x377eb8, 0x4daf4a, 0x984ea3, 0xff7f00, 0xffff33]

    circonscription_geometries = load_circonscription_geometries()
    scene = draw_map_and_graph(circonscription_geometries,
                               ra_edges,
                               lambda id: id[:2] not in dpts,
                               lambda id: sg.Color(*rgb(palette[classes[id]])))
    return scene


def test_redistrict_merge(connected=False):
    from random import randrange

    circos = redistrict_merge_connected(randrange(130, 450)) if connected\
        else redistrict_merge(randrange(130, 450))

    total_nbs = {}
    nb_circo_per_dpt = {}
    old_nb_circo_per_dpt = {}
    for _, circ in circos.items():
        d = circ["code_dpt"]
        if d not in total_nbs:
            total_nbs[d] = [0] * len(circ["t1"])
        if d not in nb_circo_per_dpt:
            nb_circo_per_dpt[d] = 0
        nb_circo_per_dpt[d] += 1
        total_nbs[d] = [a + b for a, b in zip(total_nbs[d], circ["t1"])]

    circonscriptions = load_circonscriptions()

    for _, circ in circonscriptions.items():
        dpt = circ["code_dpt"]
        if dpt not in old_nb_circo_per_dpt:
            old_nb_circo_per_dpt[dpt] = 0
        old_nb_circo_per_dpt[dpt] += 1
        assert dpt in total_nbs, dpt
        total_nbs[dpt] = [a - b for a, b in zip(total_nbs[dpt], circ["t1"])]

    for d, l in total_nbs.items():
        for v in l:
            assert v == 0, d

    if False:
        print("Réduction globale : {} %".format(
            100 * (len(circonscriptions) - len(circos)) / (len(circonscriptions))))
        for d in sorted(old_nb_circo_per_dpt):
            print("{} : {} → {} (réduction : {} %)".format(
                d, old_nb_circo_per_dpt[d],
                nb_circo_per_dpt[d],
                100 * (old_nb_circo_per_dpt[d] - nb_circo_per_dpt[d]) / old_nb_circo_per_dpt[d]))


def redistrict_map(connected=False):
    from utils.maps import draw_map
    from seagull import scenegraph as sg

    palette = [0xe41a1c, 0x377eb8, 0x4daf4a, 0x984ea3, 0xff7f00, 0xffff33]
    fallback_palette = [0x7fc97f, 0xbeaed4, 0xfdc086, 0xffff99, 0x386cb0, 0xf0027f]
    circos = redistrict_merge_connected(404) if connected else redistrict_merge(404)

    possible_colors = {}
    for code_circo in circos:
        for code_init_circo in code_circo.split('+'):
            possible_colors[code_init_circo] = set(palette)

    nb_disconnected = 0
    colors = {}
    circonscription_edges = load_circonscription_edges()
    for code_circo in circos:
        try:
            nb_disconnected += int(not is_connected(circonscription_edges,
                                                    [c for c in code_circo.split('+')]))
        except KeyError:
            pass
        current_possible_colors = set(palette)
        for code_init_circo in code_circo.split('+'):
            current_possible_colors.intersection_update(possible_colors[code_init_circo])
        if current_possible_colors:
            current_color = choice(list(current_possible_colors))
        else:
            current_color = choice(fallback_palette)
        for code_init_circo in code_circo.split('+'):
            colors[code_init_circo] = current_color
            if code_init_circo in circonscription_edges:
                for code_init_circo2 in circonscription_edges[code_init_circo]:
                    possible_colors[code_init_circo2].discard(current_color)

    print('{} circonscriptions non connexes...'.format(nb_disconnected))

    circonscription_geometries = load_circonscription_geometries()
    return draw_map(circonscription_geometries,
                    lambda id: id.startswith('Z'),
                    lambda id: sg.Color(*rgb(colors[id])),
                    draw_dep=True)


def test_nb_disconnected(connected=False):
    circonscription_edges = load_circonscription_edges()
    circos = redistrict_merge_connected(404) if connected else redistrict_merge(404)
    nb_disconnected = 0
    for code_circo in circos:
        try:
            nb_disconnected += int(not is_connected(circonscription_edges,
                                                    [c for c in code_circo.split('+')]))
        except KeyError:
            pass

    return nb_disconnected


def redistrict_cantons(target, deal_with_FdE=True):
    cantons = load_cantons()
    circonscriptions = load_circonscriptions()
    if not deal_with_FdE:
        return redistrict_merge(target, input_dist=cantons, circ_kw="canton")
    else:
        # First we exclude the (unique) canton FdE
        new_cantons = {k: v for k, v in cantons.items() if not k.startswith('0ZZ')}
        assert len(cantons) - len(new_cantons) == 1
        # Then we redistrict
        redistricted_circ = redistrict_merge(target - 11, input_dist=new_cantons, circ_kw="canton")
        # Then we add the initial 11 circonscriptions for FdE (that should be untouched)
        redistricted_circ.update({k: v for k, v in circonscriptions.items() if k.startswith('ZZ')})
        assert len(redistricted_circ) == target
        return redistricted_circ


def redistrict_cantons_connected(target, deal_with_FdE=True):
    cantons = load_cantons()
    circonscriptions = load_circonscriptions()
    from data.DataLoader import cantons_edges
    if not deal_with_FdE:
        return redistrict_merge_connected(target, input_dist=cantons,
                                          input_edges=cantons_edges, circ_kw="canton")
    else:
        # First we exclude the (unique) canton FdE
        new_cantons = {k: v for k, v in cantons.items() if not k.startswith('0ZZ')}
        assert len(cantons) - len(new_cantons) == 1
        # Then we redistrict
        redistricted_circ = redistrict_merge_connected(target - 11, input_dist=new_cantons,
                                                       input_edges=cantons_edges, circ_kw="canton")
        # Then we add the initial 11 circonscriptions for FdE (that should be untouched)
        redistricted_circ.update({k: v for k, v in circonscriptions.items() if k.startswith('ZZ')})
        assert len(redistricted_circ) == target
        return redistricted_circ


def test_redistrict_cantons(connected=False):
    from random import randrange
    circonscriptions = load_circonscriptions()

    circos = redistrict_cantons_connected(randrange(120, 450)) if connected\
        else redistrict_cantons(randrange(120, 450))

    total_nbs = {}
    for _, circ in circos.items():
        d = circ["code_dpt"]
        if d not in total_nbs:
            total_nbs[d] = [0] * len(circ["t1"])
        total_nbs[d] = [a + b for a, b in zip(total_nbs[d], circ["t1"])]

    for _, circ in circonscriptions.items():
        dpt = circ["code_dpt"]
        assert dpt in total_nbs, dpt
        total_nbs[dpt] = [a - b for a, b in zip(total_nbs[dpt], circ["t1"])]

    for d, l in total_nbs.items():
        for v in l:
            assert v == 0, d


def redistrict_map_cantons(connected=False):
    from utils.maps import draw_map
    from seagull import scenegraph as sg

    canton_edges = load_canton_edges()
    palette = [0xe41a1c, 0x377eb8, 0x4daf4a, 0x984ea3, 0xff7f00, 0xffff33]
    fallback_palette = [0x7fc97f, 0xbeaed4, 0xfdc086, 0xffff99, 0x386cb0, 0xf0027f]
    circos = redistrict_cantons(404) if not connected else redistrict_cantons_connected(404)

    possible_colors = {}
    for code_circo in circos:
        for code_init_circo in code_circo.split('+'):
            possible_colors[code_init_circo] = set(palette)

    nb_disconnected = 0
    colors = {}
    for code_circo in circos:
        try:
            nb_disconnected += int(not is_connected(canton_edges,
                                                    [c for c in code_circo.split('+')]))
        except KeyError:
            pass
        current_possible_colors = set(palette)
        for code_init_circo in code_circo.split('+'):
            current_possible_colors.intersection_update(possible_colors[code_init_circo])
        if current_possible_colors:
            current_color = choice(list(current_possible_colors))
        else:
            current_color = choice(fallback_palette)
        for code_init_circo in code_circo.split('+'):
            colors[code_init_circo] = current_color
            if code_init_circo in canton_edges:
                for code_init_circo2 in canton_edges[code_init_circo]:
                    possible_colors[code_init_circo2].discard(current_color)

    print('{} circonscriptions non connexes...'.format(nb_disconnected))

    canton_geometries = load_canton_geometries()

    return draw_map(canton_geometries,
                    lambda id: id.startswith('9'),
                    lambda id: sg.Color(*rgb(colors[id])),
                    draw_dep=True)
