import warnings


def open_xlsx(filename):
    from openpyxl import load_workbook
    with warnings.catch_warnings():
        warnings.simplefilter('ignore')
        wb = load_workbook(filename)
    return wb
