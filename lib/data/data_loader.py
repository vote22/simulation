
try:  # are we in a module?
    __file__
except NameError:  # if not, change path to root
    import os
    try:
        CWD
    except NameError:
        CWD = os.getcwd()
    os.chdir(os.path.join(CWD, ".."))

import json
import pickle

from constants import NUANCES, RESULTATS_OFFICIELS
from paths import (
    CIRCONSCRIPTION_GEOMETRIES_FILE,
    CANTON_GEOMETRIES_FILE,
    CIRCONSCRIPTION_GEOMETRIES_PICKLE,
    CANTON_GEOMETRIES_PICKLE,
    FIRST_ROUND_FILE,
    SECOND_ROUND_FILE,
    CIRCONSCRIPTIONS_FILE,
    CANTONS_FILE,
    DEPARTEMENT_GEOMETRIES_FILE,
    REDECOUPAGE_CANARD_FILE
)
from utils.misc import argmax

COLUMNS = ["inscrits", "abstention", "votants",
           "blancs", "nuls", "exprimes"] + NUANCES
L = len(COLUMNS)


################################################################################
# Helper functions (not exposed)
################################################################################

def _load_circonscriptions(circonscriptions_file):
    """
    Chargement des données des circonscriptions
    Données officielles du scrutin 2017 (vainqueurs inclus)
    """
    circonscriptions = {}

    with open(circonscriptions_file, "r") as result_file:
        next(result_file)
        for row in result_file:
            code_circo, code_dpt, dpt, circo, *words = row.strip().split('\t')
            t1, t2 = words[:L], words[L:]
            circonscriptions[code_circo] = {
                "code_dpt": code_dpt,
                "dpt": dpt,
                "circo": circo,
                "t1": [int(v) for v in t1],
                "t2": [int(v) if v != "None" else None for v in t2],
                "winner": (NUANCES[argmax([int(v) for v in t2[6:]])]
                           if t2[0] != "None"
                           else NUANCES[argmax([int(v) for v in t1[6:]])])
            }

    from collections import defaultdict

    d = defaultdict(int)
    for c in circonscriptions.values():
        d[c["winner"]] += 1
    for i, n in enumerate(NUANCES):
        assert d[n] == RESULTATS_OFFICIELS[i], n
    return circonscriptions


def _load_cantons(cantons_file):
    """
    Chargement des données des cantons
    Données officielles du scrutin 2017 (vainqueurs inclus)
    """
    cantons = {}

    with open(cantons_file, "r") as result_file:
        next(result_file)
        for row in result_file:
            code_canton, code_dpt, dpt, canton, *words = row.strip().split('\t')
            t1, t2 = words[:L], words[L:]
            cantons[code_canton] = {
                "code_dpt": code_dpt,
                "dpt": dpt,
                "canton": canton,
                "t1": [int(v) for v in t1],
                "t2": [int(v) if v != "None" else None for v in t2],
                "winner": (NUANCES[argmax([int(v) for v in t2[6:]])] if t2[0] != "None"
                           else NUANCES[argmax([int(v) for v in t1[6:]])])
            }
    return cantons


def _load_circonscriptions_edges(circonscription_geometries_pickle):
    with open(circonscription_geometries_pickle, 'rb') as f:
        return pickle.load(f)


def _load_cantons_edges(cantons_geometries_pickle):
    with open(cantons_geometries_pickle, 'rb') as f:
        return pickle.load(f)


################################################################################
# API
################################################################################

def load_circonscriptions(circonscriptions_file=CIRCONSCRIPTIONS_FILE):
    try:
        return _load_circonscriptions(circonscriptions_file)
    except FileNotFoundError:
        from data.converters.circonscriptions import create_circonscriptions_file
        create_circonscriptions_file(first_round_file=FIRST_ROUND_FILE,
                                     second_round_file=SECOND_ROUND_FILE,
                                     circonscriptions_file=CIRCONSCRIPTIONS_FILE)
        return _load_circonscriptions(circonscriptions_file)


def load_cantons(cantons_file=CANTONS_FILE):
    try:
        return _load_cantons(cantons_file)
    except FileNotFoundError:
        from data.converters.cantons import create_cantons_file
        create_cantons_file(first_round_file=FIRST_ROUND_FILE,
                            second_round_file=SECOND_ROUND_FILE,
                            circonscriptions_file=CIRCONSCRIPTIONS_FILE,
                            cantons_file=CANTONS_FILE)
        return _load_cantons(cantons_file)


def load_circonscription_geometries(
        circonscription_geometries_file=CIRCONSCRIPTION_GEOMETRIES_FILE):
    with open(circonscription_geometries_file, "r") as geometry_file:
        return json.load(geometry_file)


def load_canton_geometries(
        canton_geometries_file=CANTON_GEOMETRIES_FILE):
    with open(canton_geometries_file, "r") as geometry_file:
        return json.load(geometry_file)


def load_departement_geometries(
        departement_geometries_file=DEPARTEMENT_GEOMETRIES_FILE):
    with open(departement_geometries_file, "r") as geometry_file:
        return json.load(geometry_file)


def load_circonscription_edges(
        circonscription_geometries_file=CIRCONSCRIPTION_GEOMETRIES_FILE,
        circonscription_geometries_pickle=CIRCONSCRIPTION_GEOMETRIES_PICKLE):
    try:
        return _load_circonscriptions_edges(circonscription_geometries_pickle)
    except FileNotFoundError:
        from data.converters.graphs import create_circonscription_geometries_pickle
        create_circonscription_geometries_pickle(circonscription_geometries_file,
                                                 circonscription_geometries_pickle)
        return _load_circonscriptions_edges(circonscription_geometries_pickle)


def load_canton_edges(
        canton_geometries_file=CANTON_GEOMETRIES_FILE,
        canton_geometries_pickle=CANTON_GEOMETRIES_PICKLE):
    try:
        return _load_cantons_edges(canton_geometries_pickle)
    except FileNotFoundError:
        from data.converters.graphs import create_canton_geometries_pickle
        create_canton_geometries_pickle(canton_geometries_file,
                                        canton_geometries_pickle)
        return _load_cantons_edges(canton_geometries_pickle)


def load_redecoupage_canard(redecoupage_canard_file=REDECOUPAGE_CANARD_FILE):
    equiv_classes = {}
    nb_deputes = []
    with open(redecoupage_canard_file, "r") as redecoupage_file:
        for class_index, line in enumerate(redecoupage_file):
            row = line.strip().split(';')
            dpts = row[0].split('+')
            for dpt in dpts:
                equiv_classes[dpt] = class_index
            nb_deputes.append(int(row[1]))
    return equiv_classes, nb_deputes
