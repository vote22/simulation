try:  # are we in a module?
    __file__
except NameError:  # if not, change path to root
    import os
    try:
        CWD
    except NameError:
        CWD = os.getcwd()
    os.chdir(os.path.join(CWD, "../.."))

import json
import pickle
from shapely.geometry import shape
from paths import (
    CIRCONSCRIPTION_GEOMETRIES_FILE,
    CANTON_GEOMETRIES_FILE,
    CIRCONSCRIPTION_GEOMETRIES_PICKLE,
    CANTON_GEOMETRIES_PICKLE
    )


def _load_circonscription_geometries(circonscription_geometries_file):
    with open(circonscription_geometries_file, "r") as geometry_file:
        return json.load(geometry_file)


def _load_canton_geometries(canton_geometries_file):
    with open(canton_geometries_file, "r") as geometry_file:
        return json.load(geometry_file)


def create_circonscription_geometries_pickle(
        circonscription_geometries_file=CIRCONSCRIPTION_GEOMETRIES_FILE,
        circonscription_geometries_pickle=CIRCONSCRIPTION_GEOMETRIES_PICKLE):
    circ_edges = {}
    circonscription_geometries = _load_circonscription_geometries(circonscription_geometries_file)

    for i, feature1 in enumerate(circonscription_geometries['features']):
        ID1 = feature1["properties"]["ID"]
        shape1 = shape(feature1['geometry'])
        if ID1 not in circ_edges:
            circ_edges[ID1] = set()
        for j in range(i + 1, len(circonscription_geometries['features'])):
            feature2 = circonscription_geometries['features'][j]
            ID2 = feature2["properties"]["ID"]
            shape2 = shape(feature2['geometry'])
            if shape1.intersects(shape2):
                circ_edges[ID1].add(ID2)
                if ID2 not in circ_edges:
                    circ_edges[ID2] = set()
                circ_edges[ID2].add(ID1)

    with open(circonscription_geometries_pickle, 'wb') as f:
        pickle.dump(circ_edges, f)


def create_canton_geometries_pickle(
        canton_geometries_file=CANTON_GEOMETRIES_FILE,
        canton_geometries_pickle=CANTON_GEOMETRIES_PICKLE):
    canton_edges = {}
    canton_geometries = _load_canton_geometries(canton_geometries_file)

    for i, feature1 in enumerate(canton_geometries['features']):
        ID1 = feature1["properties"]["ID"]
        shape1 = shape(feature1['geometry'])
        if ID1 not in canton_edges:
            canton_edges[ID1] = set()
        for j in range(i + 1, len(canton_geometries['features'])):
            feature2 = canton_geometries['features'][j]
            ID2 = feature2["properties"]["ID"]
            shape2 = shape(feature2['geometry'])
            if shape1.intersects(shape2):
                canton_edges[ID1].add(ID2)
                if ID2 not in canton_edges:
                    canton_edges[ID2] = set()
                canton_edges[ID2].add(ID1)

    with open(canton_geometries_pickle, 'wb') as f:
        pickle.dump(canton_edges, f)
