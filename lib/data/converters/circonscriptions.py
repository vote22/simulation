try:  # are we in a module?
    __file__
except NameError:  # if not, change path to root
    import os
    try:
        CWD
    except NameError:
        CWD = os.getcwd()
    os.chdir(os.path.join(CWD, "../.."))


import csv
from constants import NUANCES, RESULTATS_OFFICIELS, N17, N17_T2
from paths import FIRST_ROUND_FILE, SECOND_ROUND_FILE, CIRCONSCRIPTIONS_FILE
from utils.misc import argmax
from data.xlsx import open_xlsx

from collections import defaultdict


COLUMNS = ["inscrits", "abstention", "votants", "blancs", "nuls", "exprimes"] + NUANCES
L = len(COLUMNS)


def _process_line(l):
    (code_dpt, dpt, circo, _,
     inscrits, abstention, _, votants, _,
     blancs, _, _, nuls, _, _, exprimes, _, _,
     *candidats) = (c.value for c in l)
    nuances = defaultdict(int)
    for i in range(0, len(candidats), 9):
        panneau, sexe, nom, prenom, nuance, voix, _, _, _ = candidats[i:i+9]
        if panneau is None:
            break
        assert nuance in NUANCES, nuance
        nuances[nuance] += voix
    return [
        "".join([format(code_dpt, "0>2"), format(circo, "0>3")]),
        code_dpt, dpt, circo, inscrits, abstention, votants, blancs, nuls, exprimes,
    ] + [
        nuances[n] for n in NUANCES
    ]


def _load_results(first_round_file, second_round_file):
    wb1 = open_xlsx(first_round_file)
    circo = wb1['Circo. leg. T1']
    assert circo['a1'].value == "Résultats par circonscriptions législatives"

    T1 = {}
    for i in range(N17):
        line = _process_line(circo[i+4])
        T1[line[0]] = line

    wb2 = open_xlsx(second_round_file)
    circo = wb2['Circo. leg. T2']
    assert circo['a1'].value == "Résultats par circonscriptions législatives - Tour 2"

    T2 = {}
    for i in range(N17_T2):
        line = _process_line(circo[i+4])
        assert line[0] in T1, line[0]
        T2[line[0]] = line
    return T1, T2


def _write_circonscriptions(T1, T2, circonscriptions_file):
    _, CODE_DPT, DPT, CIRCO, INSCRITS, ABSTENTION, VOTANTS, BLANCS, NULS, EXPRIMES = range(10)

    with open(circonscriptions_file, 'w') as csvfile:
        csvwriter = csv.writer(csvfile, delimiter='\t')
        csvwriter.writerow([
            '#id', 'code_dpt', 'dpt', 'circo'
        ] + [
            "%s_T%i" % (c, round)
            for round in [1, 2]
            for c in COLUMNS
        ])
        for circ in sorted(T1):
            t1 = T1[circ]
            assert t1[INSCRITS] == t1[ABSTENTION] + t1[VOTANTS], \
                "Nombre total de votants incorrects pour {}, T1".format(circ)
            assert t1[VOTANTS] == sum(t1[i] for i in [BLANCS, NULS, EXPRIMES]), \
                "Nombre total de votants incorrects pour {}, T1".format(circ)
            assert t1[EXPRIMES] == sum(t1[-len(NUANCES):]), \
                "Nombre total de votants incorrects pour {}, T1".format(circ)
            try:
                t2 = T2[circ]
            except KeyError:
                t2 = ["None" for _ in t1]
            else:
                assert t2[INSCRITS] == t2[ABSTENTION] + t2[VOTANTS], \
                    "Nombre total de votants incorrects pour {}, T2".format(circ)
                assert t2[VOTANTS] == sum(t2[i] for i in [BLANCS, NULS, EXPRIMES]), \
                    "Nombre total de votants incorrects pour {}, T2".format(circ)
                assert t2[EXPRIMES] == sum(t2[-len(NUANCES):]), \
                    "Nombre total de votants incorrects pour {}, T2".format(circ)

            csvwriter.writerow(t1 + t2[4:])


def _check_validity(circonscriptions_file):
    with open(circonscriptions_file, "r") as result_file:
        winners = [0] * len(NUANCES)
        for row in result_file:
            if row[0] == '#':
                continue
            _, _, _, _, *words = row.split('\t')
            t1, t2 = words[:L], words[L:]
            t1 = [int(u) for u in t1]
            try:
                t2 = [int(u) for u in t2]
            except ValueError:  # no second round
                winners[argmax(t1[-len(NUANCES):])] += 1
            else:
                winners[argmax(t2[-len(NUANCES):])] += 1

        for i, j, k in zip(NUANCES, winners, RESULTATS_OFFICIELS):
            assert j == k, "Le résultat diffère pour la nuance %s : %d / %d" % (i, j, k)


def create_circonscriptions_file(first_round_file=FIRST_ROUND_FILE,
                                 second_round_file=SECOND_ROUND_FILE,
                                 circonscriptions_file=CIRCONSCRIPTIONS_FILE):
    T1, T2 = _load_results(first_round_file, second_round_file)
    _write_circonscriptions(T1, T2, circonscriptions_file)
    _check_validity(circonscriptions_file)
