
try:  # are we in a module?
    __file__
except NameError:  # if not, change path to root
    import os
    try:
        CWD
    except NameError:
        CWD = os.getcwd()
    os.chdir(os.path.join(CWD, ".."))

import csv

from constants import NUANCES, N17_CANTONS, N17_CANTONS_T2
from paths import FIRST_ROUND_FILE, SECOND_ROUND_FILE, CANTONS_FILE, CIRCONSCRIPTIONS_FILE
from data.xlsx import open_xlsx

from collections import defaultdict

COLUMNS = ["inscrits", "abstention", "votants", "blancs", "nuls", "exprimes"] + NUANCES
L = len(COLUMNS)


def _process_line(l):
    (code_dpt, dpt, canton, _,
     inscrits, abstention, _, votants, _,
     blancs, _, _, nuls, _, _, exprimes, _, _,
     *candidats) = (c.value for c in l)
    nuances = defaultdict(int)
    for i in range(0, len(candidats), 4):
        nuance, voix, _, _ = candidats[i:i+4]
        if nuance is None:
            break
        assert nuance in NUANCES, nuance
        nuances[nuance] += voix
    return [
        "-".join([format(code_dpt, "0>3"), format(canton, "0>2")]),
        code_dpt, dpt, canton, inscrits, abstention, votants, blancs, nuls, exprimes,
    ] + [
        nuances[n] for n in NUANCES
    ]


def _load_results(first_round_file, second_round_file):
    wb1 = open_xlsx(first_round_file)
    cantons = wb1['Cantons T1']
    assert cantons['a1'].value == "Résultats par cantons - Tour 1"

    T1 = {}
    for i in range(N17_CANTONS):
        line = _process_line(cantons[i+4])
        T1[line[0]] = line

    wb2 = open_xlsx(second_round_file)
    cantons = wb2['Cantons T2']
    assert cantons['a1'].value == "Résultats par cantons - Tour 2"

    T2 = {}
    for i in range(N17_CANTONS_T2):
        line = _process_line(cantons[i+4])
        assert line[0] in T1, line[0]
        T2[line[0]] = line

    return T1, T2


def _write_cantons(T1, T2, cantons_file):
    _, CODE_DPT, DPT, CANTON, INSCRITS, ABSTENTION, VOTANTS, BLANCS, NULS, EXPRIMES = range(10)

    with open(cantons_file, 'w') as csvfile:
        csvwriter = csv.writer(csvfile, delimiter='\t')
        csvwriter.writerow([
            '#id', 'code_dpt', 'dpt', 'canton'
        ] + [
            "%s_T%i" % (c, round)
            for round in [1, 2]
            for c in COLUMNS
        ])
        for canton in sorted(T1):
            t1 = T1[canton]
            assert t1[INSCRITS] == t1[ABSTENTION] + t1[VOTANTS], \
                "Nombre total de votants incorrects pour {}, T1".format(canton)
            assert t1[VOTANTS] == sum(t1[i] for i in [BLANCS, NULS, EXPRIMES]), \
                "Nombre total de votants incorrects pour {}, T1".format(canton)
            assert t1[EXPRIMES] == sum(t1[-len(NUANCES):]), \
                "Nombre total de votants incorrects pour {}, T1".format(canton)
            try:
                t2 = T2[canton]
            except KeyError:
                t2 = ["None" for _ in t1]
            else:
                assert t2[INSCRITS] == t2[ABSTENTION] + t2[VOTANTS], \
                    "Nombre total de votants incorrects pour {}, T2".format(canton)
                assert t2[VOTANTS] == sum(t2[i] for i in [BLANCS, NULS, EXPRIMES]), \
                    "Nombre total de votants incorrects pour {}, T2".format(canton)
                assert t2[EXPRIMES] == sum(t2[-len(NUANCES):]), \
                    "Nombre total de votants incorrects pour {}, T2".format(canton)

            csvwriter.writerow(t1 + t2[4:])


def _check_validity(circonscriptions_file, cantons_file):
    departements = {}

    with open(circonscriptions_file, "r") as circo_file, open(cantons_file, "r") as cantons_file:
        for line in cantons_file:
            if line[0] != "#":
                _, code_dpt, _, _, *values = line.strip().split('\t')
                if code_dpt not in departements:
                    departements[code_dpt] = [0] * len(values)
                current_dpt = departements[code_dpt]
                for i, v in enumerate(values):
                    current_dpt[i] += int(v) if v != 'None' else 0
        for line in circo_file:
            if line[0] != "#":
                _, code_dpt, _, _, *values = line.strip().split('\t')
                assert code_dpt in departements
                current_dpt = departements[code_dpt]
                for i, v in enumerate(values):
                    current_dpt[i] -= int(v) if v != 'None' else 0
        for dpt in departements:
            for value in departements[dpt]:
                assert value == 0, "{}, {}".format(dpt, value)


def create_cantons_file(first_round_file=FIRST_ROUND_FILE,
                        second_round_file=SECOND_ROUND_FILE,
                        cantons_file=CANTONS_FILE,
                        circonscriptions_file=CIRCONSCRIPTIONS_FILE):
    T1, T2 = _load_results(first_round_file, second_round_file)
    _write_cantons(T1, T2, cantons_file)
    try:
        _check_validity(cantons_file, circonscriptions_file)
    except FileNotFoundError:
        from data.converters.circonscriptions import create_circonscriptions_file
        create_circonscriptions_file(first_round_file=FIRST_ROUND_FILE,
                                     second_round_file=SECOND_ROUND_FILE,
                                     circonscriptions_file=CIRCONSCRIPTIONS_FILE)
        _check_validity(cantons_file, circonscriptions_file)
