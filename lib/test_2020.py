#! /usr/bin/env python3

from data.data_loader import load_circonscriptions, load_redecoupage_canard
from rules.proportional import (
    circonscriptions_to_votes,
    proportionality_additive,
    proportionality_per_district,
    dhondt
)
from constants import RESULTATS_OFFICIELS, NUANCES
from utils.metrics import quality
from redistrict.geographic import redistrict_merge_by_function


def _redecoupage(prime_majoritaire, rep_threshold,
                 merge_key, nb_merged):
    old_circonscriptions = load_circonscriptions()
    circonscriptions = redistrict_merge_by_function(merge_key, nb_merged)
    old_votes = circonscriptions_to_votes(old_circonscriptions)
    votes = circonscriptions_to_votes(circonscriptions)
    assert sum(votes) == sum(old_votes)
    for circ in circonscriptions.values():
        circ["nb_deputes"] = circ["nb_merged"]

    proportionality_per_district(circonscriptions, prime_majoritaire, rep_threshold)

    winners_hare = [sum((circ["winners_hare"][n] for circ in circonscriptions.values()))
                    for n in range(len(NUANCES))]
    winners_dhondt = [sum((circ["winners_dhondt"][n] for circ in circonscriptions.values()))
                      for n in range(len(NUANCES))]
    min_nb_deputes = min(circ["nb_deputes"] for circ in circonscriptions.values())
    max_nb_deputes = max(circ["nb_deputes"] for circ in circonscriptions.values())
    sum_nb_deputes = sum(circ["nb_deputes"] for circ in circonscriptions.values())

    print(f"Députés entre {min_nb_deputes} et {max_nb_deputes} (total : {sum_nb_deputes})")
    print(' & '.join(NUANCES))
    print("Hare : {} → {:.2f}".format(' & '.join(str(w) for w in winners_hare),
                                      quality(winners_hare, votes)))
    print("Dhondt : {} → {:.2f}".format(' & '.join(str(w) for w in winners_dhondt),
                                        quality(winners_dhondt, votes)))
    print("=" * 80)


def test_redecoupage_canard(prime_majoritaire=0.0,
                            rep_threshold=0.0):
    equiv_classes, nb_deputes = load_redecoupage_canard()
    _redecoupage(prime_majoritaire, rep_threshold,
                 lambda circ: equiv_classes[circ["code_dpt"]],
                 lambda circo: nb_deputes[equiv_classes[circo[0]["code_dpt"]]])


def test_redecoupage(prime_majoritaire=0.0,
                     rep_threshold=0.0):
    _redecoupage(prime_majoritaire, rep_threshold,
                 lambda circ: circ["code_dpt"],
                 lambda circo: len(circo))


def test_sans_redecoupage(nbs_electeurs_par_depute=[100000, 50000, 20000, 10000, 1000, 10],
                          prime_majoritaire=0.5,
                          rep_threshold=0.05):
    results = []
    for i, nb_electeurs_par_depute in enumerate(nbs_electeurs_par_depute):
        circonscriptions = load_circonscriptions()
        votes = circonscriptions_to_votes(circonscriptions)
        for circ in circonscriptions.values():
            circ["nb_deputes"] = max(1, int(circ['t1'][0] / nb_electeurs_par_depute))

        proportionality_per_district(circonscriptions, prime_majoritaire, rep_threshold)

        winners_hare = [sum((circ["winners_hare"][n] for circ in circonscriptions.values()))
                        for n in range(len(NUANCES))]
        winners_dhondt = [sum((circ["winners_dhondt"][n] for circ in circonscriptions.values()))
                          for n in range(len(NUANCES))]
        min_nb_deputes = min(circ["nb_deputes"] for circ in circonscriptions.values())
        max_nb_deputes = max(circ["nb_deputes"] for circ in circonscriptions.values())
        avg_nb_deputes = sum(circ["nb_deputes"] for circ in circonscriptions.values()) / 577

        results.append((min_nb_deputes, max_nb_deputes, avg_nb_deputes,
                        quality(winners_hare, votes),
                        quality(winners_dhondt, votes)))
        print(f'\r{nb_electeurs_par_depute}', end='')
    return results
#
#        
#        print(f"Députés entre {min_nb_deputes} et {max_nb_deputes} (total : {sum_nb_deputes})")
#        print("Hare : {} → {:.2f}".format(winners_hare, quality(winners_hare, votes)))
#        print("Dhondt : {} → {:.2f}".format(winners_dhondt, quality(winners_dhondt, votes)))
#        print("=" * 80)
#

def test_proportionalite_additive():
    circonscriptions = load_circonscriptions()
    votes = circonscriptions_to_votes(circonscriptions)

    settings = []

    for i, ratio_prop in enumerate([0, 0.1, 0.15, 0.2, 0.3]):
        winners = proportionality_additive(
            circonscriptions,
            int(ratio_prop / (1 - ratio_prop) * len(circonscriptions)),
            0.00,
            RESULTATS_OFFICIELS,
            dhondt)
        assert sum(winners) == (
            sum(RESULTATS_OFFICIELS) + int(ratio_prop / (1 - ratio_prop) * len(circonscriptions)))
        settings.append((winners, votes, "Taux de proportionalité : %d %%" % (100 * ratio_prop)))

    for winners, votes, label in settings:
        print("{} → {:.2f}".format(label, quality(winners, votes)))


def draw_canard_map():
    from data.data_loader import (
        load_circonscriptions,
        load_circonscription_geometries,
    )
    from random import choice, shuffle
    from utils.maps import draw_map
    from utils.misc import rgb
    from seagull import scenegraph as sg
    from seagull.xml.serializer import serialize

    districts = load_circonscriptions()
    district_geometries = load_circonscription_geometries()
    equiv_classes, nb_deputes = load_redecoupage_canard()

    min_ec, max_ec = min(equiv_classes.values()), max(equiv_classes.values())

    palette = [0xe41a1c, 0x377eb8, 0x4daf4a, 0x984ea3, 0xff7f00, 0xffff33]
    colors = [sg.Color(*rgb(choice(palette))) for _ in range(min_ec, max_ec + 1)]

    colors = [sg.Color(i / max_ec, i / max_ec, i / max_ec) for i in range(min_ec, max_ec + 1)]
    shuffle(colors)

    print(serialize(draw_map(district_geometries,
                             lambda id: id.startswith('Z'),
                             lambda id: colors[equiv_classes[districts[id]["code_dpt"]]],
                             draw_dep=True)))


if __name__ == "__main__":
#    prime_majoritaire = 0.1
#    for rep_threshold in [0, 0.05, 0.1, 0.2]:
#        print(f'Rep. threshold: {rep_threshold}')
#        test_redecoupage_canard(prime_majoritaire=prime_majoritaire,
#                                rep_threshold=rep_threshold)
    x_values = list(range(1000, 70000, 1000))
    results = test_sans_redecoupage(x_values,
                                    prime_majoritaire=0.01,
                                    rep_threshold=0.05)

    min_nb_deputes = [result[0] for result in results]
    max_nb_deputes = [result[1] for result in results]
    avg_nb_deputes = [result[1] for result in results]
    winners_hare = [result[3] for result in results]
    winners_dhondt = [result[4] for result in results]

    import matplotlib.pyplot as plt

    fig, ax1 = plt.subplots()

    ax1.set_xlabel("Nb d'électeurs par député")
    ax1.set_ylabel('Indice de proportionalité')
    ax1.plot(x_values, winners_hare, label='Hare')
    ax1.plot(x_values, winners_dhondt, label="d'Hondt")
    ax1.tick_params(axis='y')
    ax1.legend()

    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

    color = 'tab:grey'
    ax2.set_ylabel('Nb moyen de députés / circ.', color=color)
    ax2.plot(x_values, avg_nb_deputes, color=color)
    ax2.tick_params(axis='y', labelcolor=color)

    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig("graph.svg")
    print()
